Each player rolls a die and moves the number of spaces on the face of the die (1-6)
• If the player lands on a ladder, the player moves up to the space at the top of the
ladder
• If the player lands on a chute, the player moves down to the space at the bottom of
the chute
• Winner must land on 100 exactly, so if player is at square 97
• rolling a 1 would move the player down to 78
• rolling a 2 would move the player to 99
• rolling a 3 would move the player to 100, winning the game
• rolling a 4, 5, or 6 would cause the player to remain at square 97
• Player will play against the computer
• Allow the player to enter a specific value from 1-6 (in order to test) or simply hit
ENTER, which causes the program to generate a random roll for the player