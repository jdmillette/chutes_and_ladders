import random

def dice_roll():
    roll = random.randint(1,6)
    return roll

def board():
    squares = [number for number in range(1,101)]
    return squares


def chutes_or_ladders(square):
    ladders = {1: 38, 4: 14, 9: 31, 21: 42, 28: 84, 36: 44, 51: 67, 71: 91, 80: 100}
    chutes= {16: 6, 47: 26, 49: 11, 56: 53, 64: 60, 87: 24, 93: 73, 95: 75, 98: 78}

    if square in ladders.keys():
        square = ladders[square]
    elif square in chutes.keys():
        square = chutes[square]
    return square


def one_hundred(squares, placement, roll):
    if placement + roll > 100:
        roll = 0
    return roll
    

def player1(squares, placement, roll=dice_roll()):
    roll = one_hundred(squares, placement, roll)
    placement = placement
    placement += roll
    placement = chutes_or_ladders(placement)
    
    square = squares[placement-1]
    placement = chutes_or_ladders(square)
    return placement


def game_play(next_roll=input):
    placement = 0
    while True:
        next_move = next_roll('Press enter to continue or type e to exit: ')
        if next_move == '' or next_move != 'e':
            position = player1(squares=board(), placement=placement, roll=dice_roll())
            print(position)
            placement = position
            if position == 100:
                print('You win!')
        elif next_move == 'e':
            print("Sorry you don't want to finish!")
            break


# game_play()