import play

def mock_dice_roll(*args):
    result = 4
    return result

def test_dice_roll(mocker):
    mocker.patch('random.randint', mock_dice_roll)
    result = play.dice_roll()
    assert result == 4


def test_player1(mocker): 
    position = play.player1(squares=[1, 2, 3, 4, 5, 16], placement=0, roll = 2)
    assert position == 2


def test_player1_chutes():
    position = play.player1(squares=[1, 2, 3, 4, 5, 16,17], placement=0, roll = 6)
    assert position == 6


def test_player1_ladder():
    position = play.player1(squares=mock_square_data(), placement=0, roll = 4)
    assert position == 14

def mock_square_data():
    squares = [square for square in range(1,101)]
    return squares

def test_one_hundred():
    position = play.player1(squares=mock_square_data(), placement=94, roll = 6)
    assert position == 100

def test_over_one_hundred():
    position = play.player1(squares=mock_square_data(), placement=96, roll = 6)
    assert position == 96

def test_under_one_hundreed():
    position = play.player1(squares=mock_square_data(), placement=95, roll = 4)
    assert position == 99

# def test_game_play():
#     def mock_next_move(prompt):
#         return ""
#     play.game_play(roll=6)
